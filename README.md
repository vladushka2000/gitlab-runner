# gitlab-runner

<https://docs.gitlab.com/runner/install/kubernetes.html>

```bash
./pull.sh
```

<https://docs.gitlab.com/ee/ci/runners/new_creation_workflow.html#installing-gitlab-runner-with-helm-chart>

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: gitlab-runner-secret
  namespace: gitlab-runner
type: Opaque
data:
  runner-registration-token: ""
  runner-token: ""
```

```bash
touch secret.yaml
printf "%s" "glrt-uHiMwhMBUBMeL7eer_-8" | base64
```

```bash
kubectl create namespace gitlab-runner
```

```bash
kubectl apply -f secret.yaml
```

```yaml
#https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/#applications
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: gitlab-runner
  namespace: argocd
spec:
  destination:
    namespace: gitlab-runner
    server: https://kubernetes.default.svc
  project: default
  source:
    repoURL: <gitlabRunnerForkURL>
    targetRevision: main
    path: bundle
    directory:
      recurse: true
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
    syncOptions:
      - CreateNamespace=true
      - Replace=true
      - ApplyOutOfSyncOnly=true
```
